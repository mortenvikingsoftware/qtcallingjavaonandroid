package com.vikingsoftware.blog.callJava;

import java.util.Random;

import android.content.Context;


class SomeJavaCode
{

    private Context context;

    public SomeJavaCode(Context context)
    {
        this.context = context;
    }

    public double add(double lhs, int rhs)
    {
        return lhs+rhs;
    }

    public void sayIt( String phrase)
    {
        System.out.println(phrase);
    }

    public String randomExcuse()
    {
        Random rand = new Random();
        String[] excuses = new String[] {"Solar Flares","Earth Rays","Global Warming","Nuclear Winter","Presidential Visit from Elbonia"};
        return (excuses[ rand.nextInt(excuses.length)]);
    }
}
