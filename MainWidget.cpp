#include "MainWidget.h"
#include "ui_MainWidget.h"

#include <QDebug>

#include <QAndroidJniObject>
#include <QtAndroid>


MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWidget)
{
    ui->setupUi(this);

    QAndroidJniObject *javaInstance = new QAndroidJniObject(
                "com/vikingsoftware/blog/callJava/SomeJavaCode"
                , "(Landroid/content/Context;)V"
                , QtAndroid::androidContext().object());


    connect(ui->addButton, &QPushButton::clicked, this, [=](){
        double lval = ui->lhs->value();
        int rval = ui->rhs->value();

        jdouble r = javaInstance->callMethod<jdouble>(
                    "add",
                    "(DI)D",
                    lval, rval);
        qDebug() << "R =" << r;
      });


    connect( ui->sayItButton, &QPushButton::clicked, [=](){
        QAndroidJniObject myJavaString = QAndroidJniObject::fromString(ui->string->text());
        javaInstance->callMethod<void>(
                    "sayIt",
                    "(Ljava/lang/String;)V",
                    myJavaString.object<jstring>());
    });

    connect(ui->pushButton, &QPushButton::clicked, [=]()
    {
        QAndroidJniObject excuse = javaInstance->callObjectMethod(
                    "randomExcuse",
                    "()Ljava/lang/String;");
        qDebug() << "Excuse was: " << excuse.toString();
    });
}

MainWidget::~MainWidget()
{
    delete ui;
}
